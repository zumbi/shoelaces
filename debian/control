Source: shoelaces
Section: devel
Priority: optional
Maintainer: Raúl Benencia <rul@kalgan.cc>
Uploaders: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>,
Build-Depends: debhelper (>= 12),
               dh-golang,
               golang-any,
               golang-github-go-kit-kit-dev,
               golang-github-gorilla-mux-dev,
               golang-github-justinas-alice-dev,
               golang-github-namsral-flag-dev,
               golang-yaml.v2-dev,
               scdoc,
Standards-Version: 4.4.0
Homepage: https://github.com/thousandeyes/shoelaces
Vcs-Browser: https://salsa.debian.org/rul-guest/shoelaces
Vcs-Git: https://salsa.debian.org/rul-guest/shoelaces.git
XS-Go-Import-Path: github.com/thousandeyes/shoelaces
Testsuite: autopkgtest-pkg-go

Package: shoelaces
Architecture: any
Built-Using: ${misc:Built-Using},
Pre-Depends: ${misc:Pre-Depends},
Depends: libjs-bootstrap4,
         libjs-jquery,
         lsb-base,
         ${misc:Depends},
         ${shlibs:Depends},
Description: automated server bootstrapping
 Shoelaces provides a mechanism for automating the bootstrapping of servers.
 It serves iPXE boot scripts, cloud-init configurations and, actually, any kind
 of plain text files.
 .
 Shoelaces main features are:
   - automation of the boot script to serve for a given server based on its IP
     address or DNS PTR record.
   - usage of Go templates to serve the configurations, allowing the user to
     customize them with parameters received via GET request.
   - web UI to show the current configurations, and history of servers that
     booted.
